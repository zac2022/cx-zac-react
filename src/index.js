import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { MoralisProvider } from "react-moralis";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";

//const serverUrl = "https://7q2hw278ilrg.usemoralis.com:2053/server";
//const appId = "5SrZ5BuBMTy3lxx7NfBoIk0BHAoExNboADhggeUh";
const serverUrl = "https://6tdxhzw6f0ea.usemoralis.com:2053/server";
const appId = "0pyN7kJBmqn6e9Sxvv6jgbcxMpd1QRfcM2TFz8gS";

const theme = extendTheme({
  config: {
    initialColorMode: "dark",
  },
});

ReactDOM.render(
  <React.StrictMode>
    <MoralisProvider appId={appId} serverUrl={serverUrl}>
      <ChakraProvider theme={theme}>
        <App />
      </ChakraProvider>
    </MoralisProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
