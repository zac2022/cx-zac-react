import { useMoralis } from "react-moralis";
import {
  Button,
  Container,
  Heading,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  CloseButton,
} from "@chakra-ui/react";

function App() {
  const { authenticate, isAuthenticated, isAuthenticating, authError, logout } =
    useMoralis();

  if (isAuthenticated) {
    return (
      <Container>
        <Heading>React Blockchain App with CX!</Heading>
        <Button onClick={() => logout()}>Logout</Button>
      </Container>
    );
  }

  return (
    <Container>
      <Heading>React Blockchain app with CX!</Heading>
      {authError && (
        <Alert status="error">
          <AlertIcon />
          <AlertTitle mr={2}>Authenticate failed</AlertTitle>
          <AlertDescription>{authError.message}</AlertDescription>
          <CloseButton position="absolute" right="8px" top="8px" />
        </Alert>
      )}
      <Button isLoading={isAuthenticating} onClick={() => authenticate()}>
        Login
      </Button>
    </Container>
  );
}

export default App;
